# Tips for selecting guitars to convert
## Consult a luthier before buying an instrument, especially used instruments!
- there are many subtleties that players are not be aware of. Have the guitar inspected in person, otherwise there is always a risk
### General Warnings
- one of the trickiest things that is difficult to know before inspecting is the quality of the fretboard (what type of wood is it and what type of cut is it i.e. quartersawn).
- Just as important, it is *very* difficult to know what type of glue was used. Cheaper guitars will use lower quality glues that are nigh impossible to unglue. Which means the fretboard removal will be much more tedious: 
    - the fretboard won't come off in one piece, and will require extra work to remove and reflatten the top of the neck (from where it was removed)
## Consult others who have had a guitar by the same brand (or ideally the same model)

### What to check
- intonation (especially if on an acoustic instrument, will require extra compensation on positioning of the Kite Guitar fretboard to replace the current one
- amount of relief on the guitar: aka consider the thicknesss/material of fretboard and the string gauges (i.e. how much tension)
    - want to be able to have the right amount of relief when the guitar is converted! Too much relief can be compensated for with a truss rod (within reason), and too little relief can be slightly compensated for with thicker strings (higher tension). Better to avoid having any backbow, however

- dimensions of the neck, is the string spacing that the prospective player would want? changing the width is possible but not easy, especially if you want there to be total consistency in the finish on the neck
    - can potentially put on an extra wide fretboard (and potentially use a filler such as bondo to smooth the contour/ difference in the curve of the neck / width of fretboard) 
 
- radius (beware, some fretboards have multiple radius aka compound)
    - the only one you have to honor is at the bridge (Unless you replace the bridge or it has adjustable heights)
- 
### Buying used:
- there are many existing instruments that need fretwork done (aka instruments that got played a lot, and the frets buzz or the frets have such little 'life' that they need. This greatly devalues a used guitar, and should mean

### Buying New:
- especially from an unknown brand, there is high risk that the cost of lutherie work will increase based on the difficulty of dealing with the conversion of poor quality instruments. So beware that when buying a cheap (especially new) instrument, you get what you pay for!
    - the best way to know if a cheap guitar is going to be more costly is to confirm with someone who has already acquired and converted such a guitar (or at least removed the fretboard)
