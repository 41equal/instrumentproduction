-- Copyright Jim Snow 2009-2021
--
-- License terms are those of General Public License Version 2, as can be
-- found at https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html or
-- otherwise dowloaded from the Free Software Foundation.

-- This file is an assorted collection of mostly-simple routines for dealing
-- with alternative tuning systems, mostly just intonation.
--
-- There is deliberately very little organization; it should be interpretted
-- more as a notebook of useful things than a coherent library or program.
-- It is normally run by loading it in ghci and invoking routines from a REPL.


{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where
import Data.List
import Control.Monad
import Text.Printf
import Data.Function(on)
import Data.Maybe(mapMaybe, fromJust)
import qualified Data.Set as S
import qualified Data.Map as M
import Data.Ord(comparing)
import Debug.Trace

divisor :: Integer -> Integer -> Bool
divisor a b = mod a b == 0

isPrime :: Integer -> Bool
isPrime 1 = True
isPrime 2 = True
isPrime n = all (\x -> not $ divisor n x) [2..(n-1)]

primes :: [Integer]
primes = filter isPrime [1..]

normalize :: (Integer,Integer) -> (Integer,Integer)
normalize (a,b)
 | divisor == 1 = (a,b)
 | otherwise = normalize (div a divisor, div b divisor)
 where divisor = gcd a b 

tones primelimit factorlimit =
 nub $ (takeWhile (<= primelimit) primes) ++ [1..factorlimit]

intervals :: [Integer] -> [(Integer,Integer)]
intervals tones =
 uniqs
 where 
   intervals = (1,16) : [ (a,b) | a <- tones, b <- tones, a <= b, b >= a*2]
   uniqs = nubBy same $ map normalize intervals

-- scales used by various experimental instruments

justintervals :: [Interval]
justintervals = [(1,1), (16,15), (10,9), (9,8), (8,7), 
                 (7,6), (6,5), (5,4), (9,7), (4,3), 
                 (7,5), (10,7), (3,2), (14,9), (8,5), 
                 (5,3), (12,7), (7,4), (16,9), (9,5), 
                 (15,8), (2,1) ]

justintervals2 :: [Interval]
justintervals2 = [(1,1), (16,15), (10,9), (9,8), (7,6), (6,5),
                  (5,4), (4,3), (7,5), (10,7), (3,2), (8,5), (5,3),
                  (7,4), (9,5), (15,8), (2,1), 
                  (9,4), (12,5), (5,2), (8,3), (14,5), (3,1), (16,9) ]

telescale :: [Interval]
telescale = [(1,1), (16,15), (10,9), (9,8),
             (7,6), (6,5), (5,4), (4,3),
             (7,5), (10,7), (3,2), (8,5),
             (5,3), (7,4), (16,9),
             (15,8), (2,1), 
             (9,4), (12,5), (5,2), (8,3), (14,5), (3,1)]

simplescale :: [Interval]
simplescale = [(1,1), (16,15), (10,9), (9,8),
               (7,6), (6,5), (5,4), (4,3),
               {- (7,5), (10,7), -}
               (45,32), (64,45),
               (3,2), (8,5),
               (5,3), (16,9),
               (15,8), (2,1),
               (32,15), (9,4), (12,5), (5,2), (8,3), (3,1), (16,5), (10,3)]

{-
aug_telescale :: [Interval]
aug_telescale = sort ((135,128):telescale)
-}

bassscale :: [Interval]
bassscale =
  [(1,1),   (16,15), (10,9), (9,8),
   (7,6),   (6,5),   (5,4),  (4,3),
   (45,32), (64,45),
   (3,2),   (8,5),   (5,3), (12,7),
   (16,9),  (9,5),   (15,8),
   (2,1),   (32,15), (9,4), 
   (12,5),  (5,2),   (8,3),
   (3,1),   (16,5),  (10,3),
   (32,9),  (15,4),  (4,1)]

fivelimitscale :: [Interval]
fivelimitscale = 
  [(1,1),   (16,15), (10,9), (9,8),
   (75,64), (6,5),   (5,4),  (4,3),
   (45,32), (64,45),
   (3,2),   (8,5),   (5,3), (128,75),
   (16,9),  (9,5),   (15,8),
   (2,1),   (32,15), (9,4),
   (12,5),  (5,2),   (8,3),
   (3,1),   (16,5),  (10,3),
   (32,9),  (15,4),  (4,1), (27,20), (40,27), (81,64), (135,128)]

fivelimitscalebolds = [0, 17, 28]

sevenlimitscale :: [Interval] =
  [(1,1),   (16,15), (10,9), (9,8),
   (8,7), (7,6), (6,5),   (5,4),  (4,3),
   (9,7),
   (7,5), (45,32), (64,45), (10,7),
   (14,9),
   (3,2),   (8,5),   (5,3), (12,7), (7,4),
   (16,9),  (9,5),   (15,8),
   (2,1),   (32,15), (9,4),
   (12,5),  (5,2),   (8,3),
   (3,1),   (16,5),  (10,3),
   (32,9),  (15,4),  (4,1), (27,20), (40,27), (81,64), (135,128), (25,24), (48,25)]


mattsscale :: [Interval] =
   [(1,1),   (16,15), (10,9), (9,8),
   (7,6), (6,5),   (5,4),  (4,3),
   (45,32), (64,45),
   (3,2),   (8,5),   (5,3), (12,7),
   (16,9),  (9,5),   (15,8),
   (2,1),   (32,15), (9,4),
   (12,5),  (5,2),   (8,3),
   (3,1),   (16,5),  (10,3),
   (32,9),  (15,4),  (4,1)]

aaronscale :: [Interval] =
   [(1,1),   (16,15), (10,9), (9,8),
   (7,6), (6,5),  (5,4),  (4,3),
   (45,32), (64,45),
   (3,2),   (14,9), (8,5),   (5,3), (7,4),
   (16,9),  (9,5),   (15,8),
   (2,1),   (32,15), (9,4),
   (12,5),  (5,2),   (8,3),
   (3,1),   (16,5),  (10,3),
   (32,9),  (15,4),  (4,1)]

keyboard24 :: [Interval] =
  [(1,1),   -- 0
   (16,15), -- 1

   (10,9),  -- 2
   (9,8),   -- 3
   (8,7),   -- 4
   (7,6),   -- 5
   (6,5),   -- 6
   (5,4),   -- 7

   (9,7),   -- 8
   (4,3),   -- 9
   
   (7,5),   -- 10
   (45,32), -- 11
   (1,1),   -- 12
   (64,45), -- 13
   (10, 7), -- 14

   (3,2),   -- 15
   (14,9),  -- 16

   (8,5),   -- 17
   (5,3),   -- 18
   (12,7),  -- 19
   (7,4),   -- 20
   (16,9),  -- 21
   (9,5),   -- 22

   (15,8)  -- 23
   {- (2,1) -- 24 -} ]

stick :: [Interval] = sortBy compareIval $
 [(1,1),
  --(17,16),
  (135,128),
  (16,15),
  (10,9),
  (9,8),
  (7,6),
  (6,5),
  (5,4),
  (45,32),
  --(64,45),
  (17,12),
  (4,3),
  (3,2),
  (5,3),
  (27,16),
  (8,5),
  (7,4),
  (16,9),
  (9,5),
  (15,8),
  (2,1),
  (11,8),
  (14,9),
  (13,8),
  (32,15),
  (9,4),
  (12,5),
  (5,2),
  (8,3),
  (3,1),
  (10,3),
  (16,5),
  (32,9),
  (15,4),
  (4,1)
  ]


fifths :: [Interval] = sortBy compareIval $
 [(1,1),
  (16,15),
  (10,9),
  (6,5),
  (5,4),
  (4,3),
  (64,45),
  (40,27),
{-
  (27,20),
  (45,32),
  (3,2),
  (8,5),
  (5,3), -}
  (9,5),
  (15,8),
  (2,1),
  (27,16),
  (32,27)
  ]

fifths2 :: [Interval] = sortBy compareIval $
 [(1,1),
  (16,15),
  (10,9),
  (9,8),
  (7,6),
  (14,9),
  (32,27),
  --(320,243),
  
  (6,5),

  (5,4),
  (81,64),

  (405,256),
  (4,3),
  (64,45),
  (45,32),
  (40,27),
  (3,2),

  (5,3),

  (8,5),
  (16,9),
  (9,5),
  (15,8),

  --(81,64),

  (2,1),
  (32,15),
  (20,9),
  (5,2),
  (8,3),
  (80,27),

  --(44,27),
  --(13,9),
  --(13,12),
  (12,5),
  --(19,8),
  (28,27),
  (112,81),
  (27,20),
  (56,27),
  (88,27),
  (96,27),
  (64,27),
  (27,16)
 ]

fourths :: [Interval] = sortBy compareIval $
 [(1,1),
  (16,15),
  (10,9),
  (9,8),
  --(7,6),
  --(14,9),
  (32,27),
  --(320,243),
  
  (6,5),

  (5,4),
  (81,64),

  (405,256),
  (4,3),
  (64,45),
  (45,32),
  (40,27),
  (3,2),

  (5,3),

  (8,5),
  (16,9),
  (9,5),
  (15,8),

  --(81,64),

  (2,1),
  (32,15),
  (20,9),
  (5,2),
  (8,3),
  (80,27),

  --(44,27),
  --(13,9),
  --(13,12),
  (12,5),
  --(19,8),
  --(28,27),
  --(112,81),
  (27,20),
  --(56,27),
  --(88,27),
  (96,27),
  (64,27),
  (27,16)
 ]

xv300 = map (\x -> (x, (fretpos x) * 626)) simplescale

johnston :: [Interval] =
  [(x, 16) | x <- [16..32]]


chapman_strings :: [Interval]
chapman_strings =
  [
   (16,9),
   (4,3),
   (1,1),
   (3,4),
   (9,16),
   (27,64),

   (2,9),
   (1,3),
   (1,2),
   (3,4),
   (9,8),
   (27,16)
  ]

{-
just16 =
 sort $
   [ (5,9), (5,6),  (5,4), (15,8), (45,16),
     (4,9), (2,3),  (1,1), (3,2),  (9,4),   (27,8),
            (8,15), (4,5), (6,5),  (5,9),   (27,10)]
-}

just16 :: [Interval]
just16 = [(1,1),(16,15),(10,9),(9,8),(6,5),(5,4),(4,3),(27,20),(45,32),(3,2),(8,5),(5,3),(27,16),(16,9),(9,5),(15,8)]

just13:: [Interval]
just13 = [(1,1),(16,15),(10,9),(9,8),(6,5),(5,4),(4,3),(3,2),(8,5),(5,3),(16,9),(9,5),(15,8)]

just20 = sortBy (compare `on` fretpos) (just16 ++ [(14,9), (7,4), (7,6), (21,16)])

justMajor :: [Interval]
justMajor = [(1,1), (10,9), (9,8), (5,4), (4,3), (3,2), (5,3), (15,8)]

expand :: [Interval] -> [Interval] -> [Interval]
expand notes ivals =
  nub $ sortBy (compare `on` fretpos)
      $ map pitchclass
      $ [stack note ival | note <- notes, ival <- (unison:ivals)]

just22 = expand justMajor [(3,2), (2,3), (5,4), (4,5), (6,5), (5,6)]
just30 = expand justMajor [(3,2), (2,3), (5,4), (4,5), (6,5), (5,6), (7,4)]

just22leanr =
 expand
  [(1,1), (9,8), (5,4), (4,3), (3,2), (5,3), (15,8), (45,32)]
  [(3,2), (2,3), (5,4), (4,5), (6,5), (5,6)]

just28 =
  sortBy
    (compare `on` fretpos)
    ((expand
      [(1,1), (9,8), (4,3), (3,2)]
      [(3,2), (2,3), (5,4), (4,5), (6,5), (5,6), (7,4), (8,7)]) ++ [(7,5), (30,28), (14,9), (27,14)] )

just31 = sortBy (compare `on` fretpos) $ expand majscale [(3,2), (2,3), (5,4), (4,5), (6,5), (5,6), (7,4)] ++ [(11,8), (13,8), (17,16), (19,16)]

just12 :: [Interval]
just12 = [(1,1), (10,9), (9,8), (6,5), (5,4), (4,3), (25,18), (3,2), (5,3), (8,5), (7,4), (15,8)]

just13' :: [Interval]
just13' = [(1,1), (10,9), (9,8), (6,5), (5,4), (4,3), (25,18), (3,2), (5,3), (8,5), (7,4), (16,9), (15,8)]

just14 :: [Interval]
just14 = [(1,1), (16,15), (10,9), (9,8), (6,5), (5,4), (4,3), (25,18), (3,2), (5,3), (8,5), (7,4), (16,9), (15,8)]

just15 :: [Interval]
just15 = [(1,1), (16,15), (10,9), (9,8), (7,6), (6,5), (5,4), (4,3), (45,32), (3,2), (5,3), (8,5), (7,4), (9,5), (15,8), (21,16), (14,9)]

just16_2 :: [Interval]
just16_2 = [(1,1), (16,15), (10,9), (9,8), (7,6), (6,5), (5,4), (4,3), (45,32), (3,2), (5,3), (8,5), (7,4), (9,5), (15,8), (14,9)]

ubassScale :: [Interval]
ubassScale = [(1,1), (16,15), (9,8), (6,5), (5,4), (4,3), (45,32), (3,2), (5,3), (8,5), (9,5), (15,8), (7,4), (7,6), (14,9), (21,16)]

dadgad :: [Interval]
dadgad = [(1,2), (3,4), (1,1), (4,3), (3,2), (2,1)]

eadgbe_d :: [Interval]
eadgbe_d = [(9,16), (3,4), (1,1), (4,3), (27,32), (9,4)]


eadgbe_e :: [Interval]
eadgbe_e = [(1,2), (4,3), (16,9), (64,27), (3,1), (4,1)]

eadgbe_a :: [Interval]
eadgbe_a = [(3,4), (1,1), (4,3), (16,9), (9,4), (3,1)]

-- biased towards 10,9 over 9,8...
eadgbe_g :: [Interval]
eadgbe_g = [(5,12), (5,9), (3,4), (1,1), (5,4), (5,3)]

eadgbe_g2 =
  [(5,12),
   stack (9,16) (80,81),
   stack (3,4) (80,81),
   stack (1,1) (80,81),
   stack (5,4) (80,81),
   stack (5,3) (80,81)]

eadg_e :: [Interval]
eadg_e = [(1,2), (2,3), (8,9), (32,27)]

eadgbe_a2 :: [Interval]
eadgbe_a2 =
  [(3,8),
   (1,2),
   (2,3),
   stack (9,10)  (80,81),
   (5,9),
   stack (3,2) (80,81)]

eadgbe_e2 :: [Interval]
eadgbe_e2 =
  [(1,2),
   (2,3),
   stack (9,10) (80,81),
   stack (6,5) (80,81),
   stack (3,2) (80,81),
   stack (2,1) (80,81)]

-- 81, 64
-- 16, 27

five_string_thirds :: [Interval]
five_string_thirds =
  take 5 $ iterate (stack (5,4)) (405,512)

just_kite_stick :: [Interval]
just_kite_stick =
  let intervals = (replicate 6 (5,4)) ++ (replicate 5 (5,8))
      accum f x xs =
        x : (go x [] xs)
        where
          go acc vals [] = reverse vals
          go acc vals (x:xs) =
            let newval = f x acc
            in go newval (newval:vals) xs
  in
    accum stack (9*9, 2*2*2*2*2*2*2*2) intervals

  --(take 7 $ iterate (stack (5,4)) (9*9, 2*2*2*2*2*2*2))

fretpos :: Interval -> Double
fretpos (a_,b_) =
 let a = fromIntegral a_ 
     b = fromIntegral b_
 in
     1-(b/a)

intervalsort :: [Interval] -> [Interval]
intervalsort = sortBy (compare `on` fretpos)


frets :: [Integer] -> [Double]
frets =
 sort . (map fretpos) . intervals

td' (a,b) = "<td>" ++ (show a) ++ ":" ++ (show b) ++ "</td>"

tableize intervals = unwords $ map td' intervals

td x = "<td align=right>" ++ x ++ "</td>"
tdb x = td $ "<b>" ++ x ++ "</b>"

showi :: Interval -> String
showi (a,b) = (show a) ++ ":" ++ (show b)

mapi f xs =
 go xs 0
 where
  go [] _ = []
  go (x:xs) i = (f x i) : go xs (i+1) 
    
tr intervals bolds =
 "<tr>" ++ (unwords $ mapi f intervals) ++ "</tr>"
 where
   f x i | i `elem` bolds = tdb $ showi x
             | otherwise = td $ showi x

textify :: Interval -> String
textify (a,b) = printf "%4s:%-3s" (show a) (show b)

textify_color :: Interval -> Integer -> String
textify_color (a,b) i = printf "\x1b[%dm%4s:%-3s\x1b[0m" i (show a) (show b)
--textify (a,b) = (show a) ++ ":" ++ (show b) ++ " "


texttr intervals = concat (map textify intervals)

tables intervals strings = tablesBolds intervals strings [0,17]

tablesBolds intervals strings bolds =
 unlines $ map (\x-> tr (map (stack x) intervals) bolds) (reverse strings) 

texttables intervals strings =
 unlines $ map (\x-> texttr (map (stack x) intervals)) strings

limitfilter :: Interval -> Bool
limitfilter (a,b) = (not $ mod a 81 == 0) && (not $ mod b 27 == 0)

rotatepc :: [Interval] -> Interval -> [Interval]
rotatepc xs ival =
  map (\x -> pitchclass $ stack x ival) xs

scalefilter :: [Interval] -> Interval -> Bool
scalefilter pcs ival =
  elem (pitchclass ival) pcs

genstringscale :: [Interval] -> [Interval] -> [Interval]
genstringscale scale strings =
  let swap (a,b) = (b,a)
      candidates = [candidate | string <- strings,
                                note <- scale,
                                let candidate = stack note (swap string),
                                -- scalefilter scale candidate,
                                compareIval (1,1) candidate /= GT,
                                compareIval candidate (2,1) /= GT
                   ]
  in
    nub $ sortBy compareIval candidates

chapmanfrets = genstringscale just16 chapman_strings 

scaledouble :: [Interval] -> [Interval]
scaledouble xs =
  nub $ xs ++ (map (stack (2,1)) xs)

pitchclass (a,b)
  | a < b = pitchclass (2*a, b)
  | a >= 2*b = pitchclass (a, 2*b)
  | otherwise = normalize (a,b)

texttr_color intervals greysp highlights =
  concat (map f intervals)
  where
    f note = if not $ greysp note
             then printf "%4s-%3s" "" "" --textify_color note 32
             else
               if elem (pitchclass note) highlights
               then textify_color note 1
               else textify_color note 0


texttables_color intervals strings greysp highlights =
 unlines $ reverse $ map (\x -> texttr_color (map (stack x) intervals) greysp (map pitchclass highlights)) (reverse strings) 

showln x = (show x) ++ "\n"

stack :: Interval -> Interval -> Interval
stack (a1,b1) (a2,b2) = normalize (a1*a2, b1*b2)

unstack :: Interval -> Interval -> Interval
unstack (a1,b1) (a2,b2) = normalize (a1*b2, b1*a2)

majortriad :: Interval -> [Interval]
majortriad x = map (stack x) [unison, majthird, fifth]

major7 :: Interval -> [Interval]
major7 x = map (stack x) [unison, majthird, fifth, (15,8)]

majordom7 :: Interval -> [Interval]
majordom7 x = map (stack x) [unison, majthird, fifth, (7,4)]

minortriad :: Interval -> [Interval]
minortriad x = [x, stack x minthird, stack x fifth]

superchord :: Interval -> [Interval]
superchord x = map (stack x) [(1,1), (3,2), (5,2), (7,4), (15,8), (2,1), (9,4), (11,8), (13,8), (17,8), (19,16)]

msuperchord x = map (stack x) [majthird, fifth, (15,8), (7,4), (9,8), (11,8), (13,8), (17,8), (19,16)]

type Interval = (Integer,Integer)

{-
instance Ord Interval where
  compare a b = compare (fretpos a) (fretpos b)
-}

data Ival = Ival Integer Integer deriving (Eq, Ord, Show)

intervalToIval (a,b) = Ival a b
ivalToInterval (Ival a b) = (a,b)

--instance Eq Interval where
--  (==) a b = same a b

-- Instance doesn't work, because it overlaps with Ord definition in prelude.

-- compareIval a b = compare (fretpos a) (fretpos b)

compareIval :: Interval -> Interval -> Ordering
compareIval (a1, b1) (a2, b2) = compare (a1*b2) (a2*b1)

unison       = (1,1)   :: Interval
minsecond    = (16,15) :: Interval
submajsecond = (10,9)  :: Interval
majsecond    = (9,8)   :: Interval
minthird     = (6,5)   :: Interval
majthird     = (5,4)   :: Interval
fourth       = (4,3)   :: Interval
tritone      = (7,5)   :: Interval
fifth        = (3,2)   :: Interval
minsixth     = (8,5)   :: Interval
majsixth     = (5,3)   :: Interval
minseventh   = (16,9)  :: Interval
gminseventh  = (9,5)   :: Interval
majseventh   = (15,8)  :: Interval
octave       = (2,1)   :: Interval

majscale = [unison, majsecond, majthird, fourth, fifth, majsixth, majseventh, octave]
chromscale = [unison, minsecond, submajsecond, majsecond, minthird, majthird, fourth, tritone, fifth, minsixth, majsixth, minseventh, gminseventh, majseventh, octave]

invert (1,1) = (1,1)
invert (a,b) = normalize (2*b,a)

minscale = map invert (reverse majscale)

nthharmonic n (a,b) = normalize (n*a, b)

harmonics x = map (\n -> nthharmonic n x) [1..]

sumtone (a1,a2) (b1,b2) = normalize (a1*b2 + b1*a2, a2*b2)
diftone (a1,a2) (b1,b2) = normalize (abs $ a1*b2 - b1*a2, a2*b2)

sumtones xs = nub $ allpairs sumtone xs
diftones xs = nub $ allpairs diftone xs

sumdiftones xs = nub $ (sumtones xs) ++ (diftones xs)

allpairs f [] = []
allpairs f (x:xs) = (map (f x) xs) ++ (allpairs f xs)

-- Keyboard stuff.
semitone = 2 ** (1/12)
cent = semitone ** (1/100)
emufine = semitone ** (1/64) -- emu proteus 2000 fine adjustment

emucn2 = 0 -- c-2
emucn1 = 12 -- c-1
emuc0  = 24 -- c0
emuc1  = 36 -- c1
emuc2  = 48 -- c2 is midi key 48
emuc3  = 60 -- c3
emuc4  = 72 -- c4 or middle c
emud3  = 62 -- we'll define this as 1:1; we choose d over middle c so that
            -- we'll have a symmetrical keyboard.

midiNoteName n =
  show $ (midiPitchClass n) ++ (show $ midiOctave n)
      
midiOctave n = (n `div` 12) - 2
midiPitchClass n =
  case n `mod` 12 of
    0  -> " C"
    1  -> "C#"
    2  -> " D"
    3  -> "D#"
    4  -> " E"
    5  -> " F"
    6  -> "F#"
    7  -> " G"
    8  -> "G#"
    9  -> " A"
    10 -> "A#"
    11 -> " B"
    _ -> error "shouldn't happen -- mod in haskell is sane"

-- Scale designed for keyboard, using 24 notes as the octave.
justKeyboard = (map (stack (1,8)) keyboard24) ++ keyboard24 ++ (map (stack (2,1)) keyboard24)
justKeyboardMidiNotes = [emud3..]

-- Produce a tuning table for EMU Proteus 2000.
genKeyboard =
  let keys = zip justKeyboard justKeyboardMidiNotes
  in
    forM_ 
      keys
      (\(note, number) -> 
        putStrLn $ (show number) ++ " " ++ (midiNoteName number) ++ " " ++ (show $ noteToEmu note) ++ " -- " ++ (show note)
      )


-- The EMU Proteus 2000 has a tuning table that expects pitch to be specified
-- as the 12-TET note + 64ths of a semitone.
noteToEmu :: Interval -> (Integer, Integer)
noteToEmu (a,b) = 
  go noteval 0 0
  where
    noteval = ((fromIntegral a) * (2**6)) / (fromIntegral b)
    go (nv::Double) coarse fine
      | nv < (1*emufine)  = (coarse, fine)
      | nv < (1*semitone) = go (nv/emufine) coarse (fine+1)
      | otherwise     = go (nv/semitone) (coarse+1) fine


-- Guess a ratio that best approximates a floating point value.
notepicker :: Double -> Integer -> Interval
notepicker freq max
  | freq <= 0  = error "invalid frequency"
  | freq > 2   = stack (2,1) (notepicker (freq/2) max)
  | freq < 0.5 = stack (1,2) (notepicker (freq*2) max)
  | otherwise  = go (1,1) (1,1) 1
  where
    go (a,b) best leasterr =
      if a > max || b > max
      then best
      else
        let ratiofreq = notefreq (a,b)
            err = (ratiofreq - freq) / freq
            abserr = abs err
        in
           if err > 0
           then if abserr < leasterr
                then go (a, b+1) (a,b) abserr
                else go (a, b+1) best leasterr
           else if abserr < leasterr
                then go (a+1, b) (a,b) abserr
                else go (a+1, b) best leasterr

notefreq :: Interval -> Double
notefreq (a,b) = (fromIntegral a) / (fromIntegral b)

notecents :: Interval -> Double
notecents (a,b) = 1200 * (logBase 2 (fromIntegral a / fromIntegral b))

-- Svg representation of note locations in a scale.
-- Ghci likes to escape all the quotes, so to get a proper file,
-- you'll want to do: writeFile "foo" $ drawscale xs
drawscale :: [(String, Double)] -> String
drawscale xs =
 "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" ++
 "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n" ++
  "<svg width=\"1000\" height=\"50\"\n" ++
    "xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink= \"http://www.w3.org/1999/xlink\">\n" ++
    "<rect width=\"1000\" height=\"50\" style=\"fill:rgb(255,255,255);stroke-width:1;stroke:rgb(255,255,255)\"/>\n" ++
    (concatMap render xs) ++
  "</svg>\n"
  where
    render (s, cents) =
      let x = cents * (1000/1200)
      in "<line x1=\"" ++ (show cents) ++ "\" y1=\"0\" " ++
         "x2=\"" ++ (show cents) ++
         "\" y2=\"50\" style=\"stroke:rgb(0,0,0);stroke-width:4\"/>\n"

styleWhite       = "fill:rgb(255,255,255); stroke-width:1; stroke:rgb(255,255,255)"
styleBlack       = "stroke:rgb(0,0,0); stroke-width:0.10"
styleGrey        = "stroke:rgb(200,200,200); stroke-width:0.10"
styleRed         = "stroke:rgb(255,0,0); stroke-width:0.10; fill:rgba(1,0,0,0.2)"
styleBlue        = "stroke:rgb(0,0,255); stroke-width:0.10; fill:rgba(0,0,1,0.2)"
styleGreen       = "stroke:rgb(0,255,0); stroke-width:0.10; fill:rgba(0,1,0,0.2)"
styleMagenta     = "stroke:rgb(255,0,255); stroke-width:0.10"
styleCyan        = "stroke:rgb(0,255,255); stroke-width:0.10"
styleGreyFill    = "stroke-width:0; fill:rgb(200,200,200)"
styleGreyOutline = "stroke-width:0.10; stroke:rgb(200,200,200); fill:rgb(255,255,255)"

styleColorFill w (sr,sg,sb) (fr,fg,fb) =
  "stroke-width:" ++ (show w) ++ ";" ++
  " stroke:rgb(" ++ (show sr) ++ "," ++ (show sg) ++ "," ++ (show sb) ++ ")" ++ ";" ++
  " fill:rgb(" ++ (show fr) ++ "," ++ (show fg) ++ "," ++ (show fb) ++ ")" ++ ";"

white = (255,255,255)
lightgrey = (230,230,230)
grey = (200,200,200)
darkgrey = (100,100,100)
black = (0,0,0)
blue = (0,0,255)
red = (255,0,0)
green = (0,255,0)
magenta = (255,0,255)
cyan = (0,255,255)
yellow = (255,255,0)

styleKey = styleColorFill 0.1 red white
styleKeyCircle = styleColorFill 0.1 green white
styleSection = styleColorFill 0.1 blue lightgrey
styleConnection = styleColorFill 0.1 green white
styleOuterBorder = styleColorFill 0.1 magenta grey
--styleTrace = styleColorFill 0.1 yellow grey

type Pt2D = (Double, Double)

svgLine :: Pt2D -> Pt2D -> String -> String
svgLine (x1, y1) (x2, y2) style =
  "<line " ++
  "x1=\"" ++ (show x1) ++ "\" y1=\"" ++ (show y1) ++ "\" " ++
  "x2=\"" ++ (show x2) ++ "\" y2=\"" ++ (show y2) ++ "\" " ++ 
  (if style == ""
   then ""
   else "style=\"" ++ style ++ "\"") ++
  "/>\n"

svgLinePoly :: [Pt2D] -> String -> String
svgLinePoly xs style =
  let lines = zip xs ((tail xs) ++ [head xs])
  in concatMap (\(a,b) -> svgLine a b style) lines

svgLines :: [Pt2D] -> String -> String
svgLines xs style =
  let lines = zip xs (tail xs)
  in concatMap (\(a,b) -> svgLine a b style) lines

svgCircle :: Pt2D -> Double -> String -> String
svgCircle (x, y) r style =
  "<circle cx=\"" ++ (show x) ++ "\" cy=\"" ++ (show y) ++
  "\" r=\"" ++ (show r) ++ "\" " ++
  (if style == ""
   then ""
   else "style=\"" ++ style ++ "\"") ++
  "/>\n"

svgLabel :: Interval -> Pt2D -> String
svgLabel (a,b) (x,y) =
  "<text " ++
  "style=\"" ++ styleRed ++ "\" " ++
  "font-size=\"3.8\" " ++
  "text-anchor=\"end\" " ++
  "x=\"" ++ (show x) ++ "\" " ++
  "y=\"" ++ (show y) ++ "\" " ++
  "transform=\"" ++
  "translate(-1, -2) " ++
  "rotate(90 " ++ (show x) ++ "," ++ (show y) ++ ")" ++
  "\" " ++
  ">" ++
  (show a) ++ "/" ++ (show b) ++ " " ++
  "</text>\n"

svgLabelSize :: Interval -> Pt2D -> Float -> String
svgLabelSize (a,b) (x,y) size =
  "<text " ++
  "style=\"" ++ styleRed ++ "\" " ++
  "font-size=\"" ++ (show size) ++ "\" " ++
  "text-anchor=\"end\" " ++
  "x=\"" ++ (show x) ++ "\" " ++
  "y=\"" ++ (show y) ++ "\" " ++
  "transform=\"" ++
  "translate(-1, -2) " ++
  "rotate(90 " ++ (show x) ++ "," ++ (show y) ++ ")" ++
  "\" " ++
  ">" ++
  (show a) ++ "/" ++ (show b) ++ " " ++
  "</text>\n"


svgLabel2 :: Interval -> Pt2D -> String
svgLabel2 (a,b) (x,y) =
  "<text " ++
  "style=\"" ++ styleRed ++ "\" " ++
  "font-size=\"3.8\" " ++
  "text-anchor=\"beginning\" " ++
  "x=\"" ++ (show x) ++ "\" " ++
  "y=\"" ++ (show y) ++ "\" " ++
  "transform=\"" ++
  "translate(-1, 22) " ++
  "rotate(90 " ++ (show x) ++ "," ++ (show y) ++ ")" ++
  "\" " ++
  ">" ++
  (show a) ++ "/" ++ (show b) ++ " " ++
  "</text>\n"

centerText :: String -> Pt2D -> String
centerText s (x,y) =
  "<text " ++
  "font-size=\"3.4\" " ++
  "text-anchor=\"middle\" " ++
  "font-family=\"serif\" " ++
  --"font-style=\"italic\" " ++
  -- "dominant-baseline=\"middle\" " ++
  "x=\"" ++ (show x) ++ "\" " ++
  "y=\"" ++ (show y) ++ "\" " ++
  "style=\"fill: #00ff00; stroke: #00ff00; stroke-width:0.10\">" ++
  s ++ " " ++
  "</text>\n"

centerTextStyle :: String -> Pt2D -> String -> String
centerTextStyle s (x,y) style =
  "<text " ++
  "font-size=\"3.4\" " ++
  "text-anchor=\"middle\" " ++
  "font-family=\"serif\" " ++
  --"font-style=\"italic\" " ++
  -- "dominant-baseline=\"middle\" " ++
  "x=\"" ++ (show x) ++ "\" " ++
  "y=\"" ++ (show y) ++ "\" " ++
  "style=\"" ++ style ++ "\">" ++
  s ++ " " ++
  "</text>\n"


svgTag :: Interval -> Pt2D -> String
svgTag (a,b) (x,y) =
  centerText ((show a) ++ "/" ++ (show b)) (x,y)

svgFraction :: Interval -> Pt2D -> String
svgFraction (a,b) (x,y) =
  concat
    [centerText (show a) (x, y-0.6),
     svgLine (x-4.0,y) (x+4.0,y) styleKeyCircle,
     centerText (show b) (x, y+3.0)]

svgFractionStyle :: Interval -> Pt2D -> String -> String
svgFractionStyle (a,b) (x,y) style =
  concat
    [centerTextStyle (show a) (x, y-0.6) style,
     svgLine (x-4.0,y) (x+4.0,y) style,
     centerTextStyle (show b) (x, y+3.0) style]


svgPoly :: [Pt2D] -> String -> String
svgPoly pts style =
  "<polygon points=\"" ++ (concatMap f pts) ++ "\" " ++
    (if style == ""
     then ""
     else "style=\"" ++ style ++ "\"") ++ "/>"
  where
    f (x,y) = (show x) ++ "," ++ (show y) ++ " "

ptAdd :: Pt2D -> Pt2D -> Pt2D
ptAdd (ax, ay) (bx, by) = (ax+bx, ay+by)

ptAdd3 :: Pt2D -> Pt2D -> Pt2D -> Pt2D
ptAdd3 (ax, ay) (bx, by) (cx, cy) = (ax+bx+cx, ay+by+cy)

ptScale :: Pt2D -> Double -> Pt2D
ptScale (x, y) scale = (x*scale, y*scale)

ptDiv :: Pt2D -> Double -> Pt2D
ptDiv pt x = ptScale pt (1/x)

ptMul :: Pt2D -> Pt2D -> Pt2D
ptMul (x1, y1) (x2, y2) = (x1*x2, y1*y2)

ptInterp :: Pt2D -> Double -> Pt2D -> Pt2D
ptInterp a scale b
  | scale < 0 = error "ptInterp scale less than 0"
  | scale > 1 = error "ptInterp: scale greater than 1"
  | otherwise = ptAdd (ptScale a (1-scale)) (ptScale b scale)

ptInterpPermissive :: Pt2D -> Double -> Pt2D -> Pt2D
ptInterpPermissive a scale b =
  ptAdd (ptScale a (1-scale)) (ptScale b scale)

infinity = 1000000000

ptMax :: Pt2D -> Pt2D -> Pt2D
ptMax (ax, ay) (bx, by) = (max ax bx, max ay by)

ptsMax :: [Pt2D] -> Pt2D
ptsMax = foldl' ptMax (-infinity, -infinity)

ptMin :: Pt2D -> Pt2D -> Pt2D
ptMin (ax, ay) (bx, by) = (min ax bx, min ay by)

ptsMin :: [Pt2D] -> Pt2D
ptsMin = foldl' ptMin (infinity, infinity)

ptLen :: Pt2D -> Double
ptLen (x,y) = sqrt $ (x*x) + (y*y)

ptLenSqr :: Pt2D -> Double
ptLenSqr (x,y) = (x*x) + (y*y)

ptSub :: Pt2D -> Pt2D -> Pt2D
ptSub (x1,y1) (x2,y2) = (x1-x2, y1-y2)

ptDist :: Pt2D -> Pt2D -> Double
ptDist a b = ptLen $ ptSub b a

ptDistX :: Pt2D -> Pt2D -> Double
ptDistX (x1,_) (x2,_) = abs $ x2-x1

ptDistY :: Pt2D -> Pt2D -> Double
ptDistY (_,y1) (_,y2) = abs $ y2-y1

ptNormalize :: Pt2D -> Pt2D
ptNormalize pt = ptScale pt (1 / ptLen pt)

ptDot :: Pt2D -> Pt2D -> Double
ptDot (x1,y1) (x2,y2) = x1*x2 + y1*y2

ptCross :: Pt2D -> Pt2D -> Double
ptCross (ax,ay) (bx, by) =
  ax*by - ay*bx

clamp :: Double -> Double -> Double -> Double
clamp a b c
  | a > b = a
  | b > c = c
  | otherwise = b

ptAngle :: Pt2D -> Pt2D -> Pt2D -> Double
ptAngle a b c =
  let ba = ptNormalize $ ptSub a b
      bc = ptNormalize $ ptSub c b
      result = acos $ clamp (-1) (ptDot ba bc) 1
  in
    if result < 0 || result > pi
    then error "something wrong"
    else result

ptAngleCw :: Pt2D -> Double
ptAngleCw pt' =
  let (x,y) = ptNormalize pt'
  in atan2 x y

-- angle in rotations
ptRotate :: Pt2D -> Double -> Pt2D -> Pt2D
ptRotate origin angle pt =
  let s = sin (angle * (2*pi))
      c = cos (angle * (2*pi))
      (x,y) = ptSub pt origin
      pt' = (x*c - y*s, x*s + y*c)
  in
     ptAdd pt' origin

lineShrink :: Pt2D -> Pt2D -> Double -> Maybe (Pt2D, Pt2D)
lineShrink p1 p2 amnt =
  if len <= amnt*2
  then
    Nothing
  else
    Just ((ptAdd p1 vscaled), (ptSub p2 vscaled)) 
  where
    v = ptSub p2 p1
    len = ptLen v
    vscaled = ptScale v (amnt/len)

delta :: Double
delta = 0.0000001

almostEq :: Pt2D -> Pt2D -> Bool
almostEq (x1,y1) (x2,y2) =
  abs (x1-x2) < delta && abs (y1-y2) < delta

almostEqD :: Double -> Double -> Bool
almostEqD a b = abs (b - a) < delta

data Intersect = Intersect Pt2D | Miss | Parallel | ColinearDisjoint | Colinear | Same deriving (Eq, Ord, Show)


segmentIntersection' :: Bool -> (Pt2D, Pt2D) -> (Pt2D, Pt2D) -> Intersect
segmentIntersection' infinite (p,pr) (q,qs) =
  let r = ptSub pr p
      s = ptSub qs q
      rxs = ptCross r s
      sxr = -rxs
      pq = ptSub q p
      qp = ptSub p q
      pqxs = ptCross pq s
      qpxr = ptCross qp r
  in
    if almostEqD rxs 0
    then
      --paralel
      if almostEqD qpxr 0
      then
        let t0 = ptDot pq r / ptDot r r
            t1 = ptDot (ptSub (ptAdd q s) p) r / ptDot r r
        in
          if infinite || (t0 < 1 && t1 > 0) || (t1 < 1 && t0 > 0)
          then
            if (almostEq p q && almostEq pr qs) ||
               (almostEq p qs && almostEq pr q)
            then Same
            else Colinear
          else ColinearDisjoint
      else
        Parallel
    else
      let t = pqxs / rxs
          u = qpxr / sxr
      in
        if infinite || (t > delta && t < (1-delta) && u > delta && u < (1-delta))
        then
          Intersect (ptAdd p (ptScale r t))
        else
          Miss

segmentIntersection = segmentIntersection' False
lineIntersection = segmentIntersection' True

filterHits :: [Intersect] -> [Pt2D]
filterHits [] = []
filterHits (Intersect p:xs) = p : filterHits xs
filterHits (_:xs) = filterHits xs

ptInside :: Pt2D -> [Pt2D] -> Bool
ptInside pt poly =
  let ray = (pt, ptAdd pt (0, 1000000.0))
      sides = zip poly (tail poly ++ [head poly])
      intersections = length $ mapMaybe (\x -> f $ segmentIntersection ray x) sides
      f (Intersect i) = Just i
      f _ = Nothing
  in
    (mod intersections 2) == 1

ptInsideCvx :: Pt2D -> [(Pt2D, Pt2D)] -> Bool
ptInsideCvx p sides =
  foldl' (&&) True (map inside sides)
  where
    inside (p1, p2) =
      let p1p2 = ptSub p2 p1
          p1p = ptSub p p1
      in
         ptCross p1p2 p1p < delta

ptInterpN :: [(Pt2D, Double)] -> Pt2D
ptInterpN pts =
  let wsum = sum (map snd pts)
  in foldl1 ptAdd [ptScale pt (w/wsum) | (pt, w) <- pts]

ptInterp2 :: (Pt2D, Double) -> (Pt2D, Double) -> Pt2D
ptInterp2 a b = ptInterpN [a,b]

ptInterp3 :: (Pt2D, Double) -> (Pt2D, Double) -> (Pt2D, Double) -> Pt2D
ptInterp3 a b c = ptInterpN [a,b,c]

ptMid :: Pt2D -> Pt2D -> Pt2D
ptMid a b = ptScale (ptAdd a b) 0.5

filternth :: (Integer -> Bool) -> [a] -> [a]
filternth f xs =
  go 0 [] xs 
  where
    go n acc [] = reverse acc
    go n acc (x:xs) =
      if (f n)
      then go (n+1) (x:acc) xs
      else go (n+1) acc xs

-- generate all pairings of a list
-- elements are not paired with themselves, or in reverse order
pairwise :: [a] -> [(a,a)]
pairwise xs =
  let len = fromIntegral $ length xs
      pairs = [(a,b) | a <- xs, b <- xs]
  in
    filternth (\n -> not $ (mod n len) <= (div n len)) pairs

-- O(N^8) or so
delaunaySolver :: (Show a, Ord a) => Double -> [(a, Pt2D)] -> [(a,a)]
delaunaySolver maxlen cells =
  let allpairs      = filter (\((_,a),(_,b)) -> ptDist a b < maxlen) (pairwise cells)
      allpairpairs  = pairwise allpairs
      longcrossings =
        concatMap
          (\lines@(((a1, apt1), (a2, apt2)), ((b1, bpt1), (b2, bpt2))) ->
              case segmentIntersection (apt1, apt2) (bpt1, bpt2) of
                Miss -> []
                Parallel -> []
                ColinearDisjoint -> []
                Same -> trace "same" []
                Intersect isect -> 
                            let apt1angle = ptAngle bpt1 apt1 bpt2
                                apt2angle = ptAngle bpt1 apt2 bpt2
                                bpt1angle = ptAngle apt1 bpt1 apt2
                                bpt2angle = ptAngle apt1 bpt2 apt2
                            in
                                if abs ((apt1angle + apt2angle + bpt1angle + bpt2angle) - (2*pi)) > delta*10
                                then error $ printf "interior angles don't add up %f %f %f %f %s %s\n" apt1angle apt2angle bpt1angle bpt2angle (show lines) (show isect)
                                else
                                  if isNaN apt1angle || isNaN apt2angle || isNaN bpt1angle || isNaN bpt2angle
                                  then
                                    if isNaN apt1angle || isNaN apt2angle
                                    then trace "nan a" [] -- $ Just (a1,a2)
                                    else trace "nan b" [] -- $ Just (b1,b2)
                                  else
                                    if apt1angle + apt2angle <= pi
                                    then [(a1,a2)]
                                    else if bpt1angle + bpt2angle <= pi + delta
                                         then [(b1,b2)]
                                         else error $ printf "shouldn't happen %f %f %f %f %s\n" apt1angle apt2angle bpt1angle bpt2angle (show lines)
                Colinear ->
                  let lena = ptLenSqr (ptSub apt1 apt2)
                      lenb = ptLenSqr (ptSub bpt1 bpt2)
                  in if lena > lena
                     then [(a1,a2)]
                     else if lenb > lena
                          then [(b1,b2)]
                          else {- trace ("overlapping equal-length segments " ++ (show lines)) [(a1,a2),(b1,b2)] -} []
          )
          allpairpairs
      --remainingpairs = deleteFirstsBy pairsame (map innerfst allpairs) longcrossings
      innerfst ((a,_), (b,_)) = (a,b)
      --pairsame (a,b) (c,d) = (samef a c) && (samef b d)
      convert   (a,b) = (intervalToIval a, intervalToIval b)
      unconvert (a,b) = (ivalToInterval a, ivalToInterval b)

      longCrossingsSet = S.fromList longcrossings
      remainingpairs = filter (\x -> S.notMember x longCrossingsSet) (map innerfst allpairs)
  in
     remainingpairs

-- Given a list of tagged points, return the delauney neighbors of
-- each point sorted clockwise.
delaunay :: (Show a, Ord a) => Double -> [(a, Pt2D)] -> [(a, Pt2D, [(a, Pt2D)])]
delaunay maxlen cells =
  let del = delaunaySolver maxlen cells
      ptMap = M.fromList cells
      neighbors =
        [(a, pt, sortBy (comparing (cw pt)) (mapMaybe
                   (\(b,c) ->
                     if a==b
                     then Just (c, fromJust $ M.lookup c ptMap)
                     else
                       if a==c
                       then Just (b, fromJust $ M.lookup b ptMap)
                       else Nothing 
                   )
                 del)) | (a,pt) <- cells]

      -- clockwise angle relative to vertical
      cw center (_, pt) = ptAngleCw (ptSub pt center)
  in
     neighbors

delaunayKeys :: Double -> [(Interval, Pt2D)] -> [(Interval, Pt2D, [(Interval, Pt2D)])]
delaunayKeys maxlen cells =
  map
    (\(c, pt, ns) ->
      (ivalToInterval c, pt, (map (\(n, npt) ->
                                    (ivalToInterval n, npt)) ns)))
    (delaunay maxlen (map (\(a,pt) -> (intervalToIval a, pt)) cells))

{-
voronoiKeys :: [(Interval, Pt2D)] -> [(Interval, Pt2D, [Interval])]
voronoiKeys cells =
  let unconvert (a,b) = (ivalToInterval a, ivalToInterval b)
  in map unconvert $ delaunay 1.5 (map (\(a,pt) -> (intervalToIval a, pt)) cells)
-}

-- 90 degrees clockwise normal vector to line
linePerp :: (Pt2D, Pt2D) -> Pt2D
linePerp (p1@(x1, y1), p2@(x2, y2)) =
  ptNormalize $ ptSub (y2,(-x2)) (y1,(-x1))

polygonShrink :: [Pt2D] -> Double -> [Pt2D]
polygonShrink pts border =
  let lines = pairs pts
      adjustedLines =
        [let perp = ptScale (linePerp (p1, p2)) border
         in (ptAdd p1 perp, ptAdd p2 perp)
          | (p1, p2) <- lines]
      newPts =
        mapMaybe
          (\(l1,l2) -> f $ lineIntersection l1 l2) 
          (zip adjustedLines ((tail adjustedLines) ++ [head adjustedLines]))
      f (Intersect i) = Just i
      f _ = Nothing
  in
    polygonFix newPts

mapAlternate :: (a -> b) -> (a -> b) -> [a] -> [b]
mapAlternate f g [] = []
mapAlternate f g (x:xs) = (f x) : mapAlternate g f xs

isHorizontal :: (Pt2D, Pt2D) -> Bool
isHorizontal ((x1, y1), (x2, y2)) =
  (abs $ y2 - y1) < delta

avgPt :: [Pt2D] -> Pt2D
avgPt pts = ptScale (foldl ptAdd (0,0) pts) (1/(fromIntegral $ length pts))

polygonFix :: [Pt2D] -> [Pt2D]
polygonFix pts =
  let center = avgPt pts
      segs =
        filter
          (\(a,b) -> ptCross (ptSub b a) (ptSub center a) < 0)
          (pairs pts)
  in polygonClip center segs []

pairs :: [a] -> [(a,a)]
pairs xs = zip xs ((tail xs) ++ [head xs])

pairsNoLoop :: [a] -> [(a,a)]
pairsNoLoop xs = zip xs (tail xs)

polygonClip :: Pt2D -> [(Pt2D, Pt2D)] -> [(Pt2D, Pt2D)] -> [Pt2D]
polygonClip center poly1 poly2 =
  let crossings = [ lineIntersection l1 l2 | (l1,l2) <- pairwise (poly1 ++ poly2)]
      verts =
        mapMaybe (\x -> case x of
                          Intersect isect ->
                            if ptInsideCvx isect poly1 && ptInsideCvx isect poly2
                            then Just isect
                            else Nothing  
                          _ -> Nothing) crossings
      cw pt = ptAngleCw (ptSub pt center)

  in sortBy (comparing cw) verts
 
lineClip :: Pt2D -> [(Pt2D, Pt2D)] -> (Pt2D, Pt2D) -> Maybe (Pt2D, Pt2D)
lineClip center poly line@(p1,p2) =
  let isHit (Intersect p) = True
      isHit _ = False
      crossings = filter isHit [ lineIntersection pl line | pl <- poly ]
      hitpt = case crossings of
               (Intersect p:xs) -> Just p
               _ -> Nothing
      p1' =
       if ptInsideCvx p1 poly
       then Just p1
       else hitpt
      p2' =
       if ptInsideCvx p2 poly
       then Just p2
       else hitpt
  in
    do p1'' <- p1'
       p2'' <- p2'
       return (p1'',p2'')

interleave :: [a] -> [a] -> [a]
interleave xs ys =
  reverse $ go1 xs ys []
  where
   go1 [] _ acc = acc
   go1 (x:xs) ys acc = go2 xs ys (x:acc)

   go2 _ [] acc = acc
   go2 xs (y:ys) acc = go1 xs ys (y:acc)

voronoiCell :: (a, Pt2D, [(a, Pt2D)]) -> (a -> Double) -> Double -> ((Pt2D -> Pt2D), (Pt2D -> Pt2D)) -> [Pt2D] -> [Pt2D]
voronoiCell (cval, center, neighbors') weightf margin (xfm,invxfm) clip =
  let scalev    = (1,1)
      cweight   = weightf cval
      neighbors = map (\(nval, n) -> ptInterp2 (center, cweight) (n, weightf nval) ) neighbors'
      outline   = map (\x -> (x, ptAdd x (ptMul (linePerp (center, x)) scalev))) neighbors
      npairs    = pairs neighbors'
      triads    = [ ptInterp3
                      (center, cweight)
                      (n1, weightf nval1)
                      (n2, weightf nval2) | ((nval1, n1), (nval2, n2)) <- npairs ]
  in
    polygonShrink (map xfm (polygonClip center outline (pairs (map invxfm clip)))) margin

svgWrap :: String -> Pt2D -> String -> String
svgWrap contents (w,h) units =
  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" ++
  "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n" ++
  "<svg width=\"" ++ (show w) ++ units ++ "\" height=\"" ++ (show h) ++ units ++ "\" " ++
    "viewBox=\"0 0 " ++ (show w) ++ " " ++ (show h) ++ "\" " ++
    "xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink= \"http://www.w3.org/1999/xlink\">\n" ++
    contents ++
  "</svg>\n"

noteOctave :: Interval -> Interval
noteOctave note =
  unstack note (pitchclass note)
    
-- Find all the notes within a range of notes that have the
-- same pitch classes as in the scale.
noteSequence :: Interval -> Interval -> [Interval] -> [Interval]
noteSequence min max notes' = reverse $ go min []
  where
    notes = nub $ sortBy compareIval $ map pitchclass notes'
    go min acc
      | fretpos min > fretpos max = acc
      | elem (pitchclass min) notes = go (next min) (min:acc)
      | otherwise = go (next min) acc

    next note =
      let oct  = noteOctave note
          pc   = pitchclass note
          mnext = find (\x -> compareIval pc x == LT) notes
      in
        case mnext of
          Nothing -> stack oct (stack (2,1) (head notes))
          Just a -> stack oct a

data FretStyle = FretNumber | FretSmallNumber | FretDot | FretInlay | FretDoubleDots | FretTripleDots | NoDecoration deriving (Eq, Ord, Show)

drawfingerboard :: [Interval] -> [Interval] -> Interval -> Double -> Double -> (Pt2D, Pt2D, Pt2D, Pt2D) -> FretStyle -> Double -> String
drawfingerboard strings scale maxnote scalelenl scalelenr (nl, nr, bl', br') fretstyle margin =
  drawfingerboard' strings scale (\_ _ -> True) maxnote scalelenl scalelenr (nl, nr, bl', br') fretstyle margin
 
drawfingerboard' :: [Interval] -> [Interval] -> (Integer -> Interval -> Bool) -> Interval -> Double -> Double -> (Pt2D, Pt2D, Pt2D, Pt2D) -> FretStyle -> Double -> String
drawfingerboard' strings scale noteSelector maxnote scalelenl scalelenr (nl, nr, bl', br') fretstyle margin =
 svgWrap (concat (outline ++ stringFrets)) (width,height) "mm"

 where
   (maxx,maxy) = ptMax (ptMax nl nr) (ptMax bl' br')
   (minx,miny) = ptMin (ptMin nl nr) (ptMin bl' br')

   width = maxx - minx + (margin*2)
   height = maxy - miny + (margin*2)
   offset = (margin,margin)
   offset2 = (margin,-margin)

   svgLine' a b s = svgLine (ptAdd a offset) (ptAdd b offset) s
   svgLabel' i p = svgLabel i (ptAdd p offset)
   svgLabel2' i p = svgLabel2 i (ptAdd p offset2)
   svgLabelSize' i p s = svgLabelSize i (ptAdd p offset) s

   decoration pitch pos = case fretstyle of
     FretNumber -> svgLabel' pitch pos
     FretSmallNumber -> svgLabelSize' pitch pos 2.0
     FretDot ->
       let pc = pitchclass pitch 
       in if pc == (1,1)
          then svgCircle (ptAdd (ptAdd pos offset) (0,-6)) 1.2 styleBlue
          else if pc == (3,2) || pc == (4,3)
               then svgCircle (ptAdd (ptAdd pos offset) (0,-6)) 1 styleBlue
               else ""
     _ -> ""

   renderString :: Interval -> Integer -> String
   renderString string stringnum =
     concatMap
       (\note -> renderFret (unstack note string) note stringnum)
       (noteSequence string (stack string maxnote) scale)

   renderFret :: Interval -> Interval -> Integer -> String
   renderFret note pitch stringNum =
     if noteSelector stringNum pitch
     then
       (svgLine' a b styleBlack) ++
       (decoration pitch (ptInterp a 0.5 b))
     else
        ""
     where
       stringNumF = fromIntegral stringNum

       anmid    = ptInterp nl (stringNumF / numStrings) nr
       abmid    = ptInterp bl (stringNumF / numStrings) br
       a        = ptInterp anmid (fretpos note) abmid

       bnmid    = ptInterp nl ((stringNumF+1) / numStrings) nr
       bbmid    = ptInterp bl ((stringNumF+1) / numStrings) br
       b        = ptInterp bnmid (fretpos note) bbmid

   nmid = ptInterp nl 0.5 nr
   bmid = ptInterp bl 0.5 br
   centerV = ptSub bmid nmid
   centerVN = ptScale centerV (1 / ptLen centerV)

   lsideV = ptSub bl' nl
   rsideV = ptSub br' nr
   bl = ptAdd nl (ptScale lsideV (scalelenl / ptLen lsideV)) 
   br = ptAdd nr (ptScale rsideV (scalelenr / ptLen rsideV))

   stringFrets =
     mapi (\s i -> renderString s i) strings
   numStrings = fromIntegral $ length strings

   ols = styleGrey
   outline = [svgLine' nl nr ols, svgLine' nr br' ols,
              svgLine' br' bl' ols, svgLine' bl' nl ols]

drawfingerboardEt :: Double -> Integer -> [Integer] -> (Integer -> Bool) -> (Integer -> Bool) -> Double -> Double -> (Pt2D, Pt2D, Pt2D, Pt2D) -> FretStyle -> Double -> String
drawfingerboardEt et frets stringOffsets markerSelector fretSelector scalelenl scalelenr (nl, nr, bl', br') fretstyle margin =
 svgWrap (concat (outline ++ fretsString)) (width,height) "mm"

 where
   (maxx,maxy) = ptMax (ptMax nl nr) (ptMax bl br)
   (minx,miny) = ptMin (ptMin nl nr) (ptMin bl br)

   width = maxx - minx + (margin*2)
   height = maxy - miny + (margin*2)
   offset = (margin,margin)
   offset2 = (margin,-margin)

   svgLine' a b s = svgLine (ptAdd a offset) (ptAdd b offset) s
   svgLabel' i p = svgLabel i (ptAdd p offset)
   svgLabel2' i p = svgLabel2 i (ptAdd p offset2)
   svgLabelSize' i p s = svgLabelSize i (ptAdd p offset) s

   strings = length stringOffsets

   decoration fretnum stringnum note = case fretstyle of
     FretNumber -> ""
     FretDot ->
       let note = fretnum + (stringOffsets!!stringnum)
       in
         if markerSelector note
         then
           let relative_x = ((fromIntegral stringnum) + 0.5) / (fromIntegral strings)
               relative_y = fretPos ((fromIntegral fretnum) - 0.8)
               pos = ptInterpPermissive (ptInterp nl relative_x nr) relative_y (ptInterp bl relative_x br)
           in
             svgCircle (ptAdd pos offset) 1.5 styleBlue
         else ""
     _ -> ""

   inlay fretnum =
     if mod fretnum 8 == 0 && fretstyle == FretInlay
     then
       let fp1 = fretPos $ fromIntegral fretnum  -- ?? the lowerfret ( higher fret number aka closer to the heel)
           fp2 = fretPos $ fromIntegral (fretnum - 2)  -- ?? the higher higher fret (lower fret number aka closer to the nut)

           a = ptAdd offset $ ptInterp nl fp1 bl  -- ?? get the bottomleft corner of the fret 
           b = ptAdd offset $ ptInterp nl fp2 bl  -- ?? get the topleft corner of the fret
           c = ptAdd offset $ ptInterp nr fp2 br  -- ?? get the topright corner of the fret 
           d = ptAdd offset $ ptInterp nr fp1 br  -- ?? get the bottomright corner of the fret 

           lmid = ptMid a b
           rmid = ptMid c d

           width = ptLen (ptSub lmid rmid)
           height = ptLen (ptSub a b) -- approximate

           center = ptMid lmid rmid

           a' = ptInterp a (0.3) center
           b' = ptInterp b (0.4) center
           c' = ptInterp c (0.35) center
           d' = ptInterp d (0.3) center

           a'' = ptAdd center (ptScale (-0.8, 0.25) height)
           b'' = ptAdd center (ptScale (0, -0.3) height)
           c'' = ptAdd center (ptScale (0.8, 0.25) height)


       in svgPoly [a'', b'', c''] styleBlue
     else
       ""

   inlay2 :: Integer -> String
   inlay2 fretnum =
     if mod fretnum 8 == 0 && not (fretnum == 0) && fretstyle == FretDoubleDots  -- ?? every four (kite frets aka 8 edo steps) put a fretinlay
     then
       let fp1 = fretPos $ fromIntegral fretnum -- ?? the lowerfret ( higher fret number aka closer to the heel)
           fp2 = fretPos $ fromIntegral (fretnum - 2) -- ?? the higher higher fret (lower fret number aka closer to the nut)

           a = ptAdd offset $ ptInterpPermissive nl fp1 bl -- ?? get the bottomleft corner of the fret 
           b = ptAdd offset $ ptInterpPermissive nl fp2 bl -- ?? get the topleft corner of the fret
           c = ptAdd offset $ ptInterpPermissive nr fp2 br -- ?? get the topright corner of the fret 
           d = ptAdd offset $ ptInterpPermissive nr fp1 br -- ?? get the bottomright corner of the fret

           midl = ptMid a b
           midr = ptMid c d

           center = ptMid midl midr
           centerr = ptInterp midl (1/4) midr
           centerl = ptInterp midl (3/4) midr

       in if mod fretnum 24 == 0
          then
            (svgCircle centerl 3 styleBlue) ++ (svgCircle centerr 3 styleBlue)
          else
            svgCircle center 3 styleBlue
     else
       ""

   inlay3 :: Integer -> String
   inlay3 fretnum =
     if mod fretnum 8 == 0 && not (fretnum == 0) && fretstyle == FretTripleDots  -- ?? every four (kite frets aka 8 edo steps) put a fretinlay
     then
       let fp1 = fretPos $ fromIntegral fretnum -- ?? the lowerfret ( higher fret number aka closer to the heel)
           fp2 = fretPos $ fromIntegral (fretnum - 2) -- ?? the higher higher fret (lower fret number aka closer to the nut)

           a = ptAdd offset $ ptInterpPermissive nl fp1 bl -- ?? get the bottomleft corner of the fret 
           b = ptAdd offset $ ptInterpPermissive nl fp2 bl -- ?? get the topleft corner of the fret
           c = ptAdd offset $ ptInterpPermissive nr fp2 br -- ?? get the topright corner of the fret 
           d = ptAdd offset $ ptInterpPermissive nr fp1 br -- ?? get the bottomright corner of the fret

           midl = ptMid a b
           midr = ptMid c d

           center = ptMid midl midr
           centerr = ptInterp midl (1/4) midr
           centerl = ptInterp midl (3/4) midr
           centerlm = ptInterp midl (3/8) midr
           centerrm = ptInterp midl (5/8) midr
           
     in if mod fretnum 24 == 0
          then
            (svgCircle centerl 3 styleBlue) ++ (svgCircle centerr 3 styleBlue) ++ (svgCircle center 3 styleBlue)
          else if mod (fretnum + 8) 24 == 0
            then
                (svgCircle centerlm 3 styleBlue) ++ (svgCircle centerrm 3 styleBlue) 
            else if mod fretnum 8 == 0
              then   
            svgCircle center 3 styleBlue
          else
            "" -- ?? svgCircle center 3 styleBlue
     else
       ""

   fretDecorations fretnum =
     mapi (\offset stringnum -> decoration fretnum stringnum (fretnum+offset)) stringOffsets

   -- distance from nut, as fraction of scale length
   fretPos fretNum = 1 - (1 / (2**((fretNum) / et)))

   renderFret :: Integer -> String
   renderFret fretNum =
     if fretSelector fretNum
     then
       let nutDistance = fretPos (fromIntegral fretNum) 
           bbl = ((fst bl'), snd (ptInterp bb_nl nutDistance bl))
           l = ptInterp nl nutDistance bl
           r = ptInterp nr nutDistance br
           bbr = ((fst br'), snd (ptInterp bb_nr nutDistance br))
       in
         (svgLine' bbl l styleRed) ++
         (svgLine' l r styleRed) ++
         (svgLine' r bbr styleRed) ++
         (concat (fretDecorations fretNum)) ++
         (inlay fretNum) ++
         (inlay2 fretNum) ++
         (inlay3 fretNum)
     else
       ""

   fretsString = map (renderFret) [0..frets]

   --nmid = ptInterp nl 0.5 nr
   --bmid = ptInterp bl 0.5 br
   --centerV = ptSub bmid nmid
   --centerVN = ptScale centerV (1 / ptLen centerV)

   lsideV = ptSub bl' nl
   rsideV = ptSub br' nr

   -- actual bridge locations, extrapolated from fingerboard edges
   bl = ptAdd nl (ptScale lsideV (scalelenl / ptLen lsideV)) 
   br = ptAdd nr (ptScale rsideV (scalelenr / ptLen rsideV))

   bb_nr = (fst br', snd nr)
   bb_nl = (fst bl', snd nl)

   ols = styleMagenta

   zfl = ptAdd nl (0, -13)
   zfr = ptAdd nr (0, -13)

   outline = [--svgLine' nl nr ols,
              svgLine' nr zfr ols,
              svgLine' zfr zfl ols,
              svgLine' zfl nl ols,
              svgLine' nr br' ols,
              svgLine' br' bl' ols,
              svgLine' bl' nl ols,
              svgLine' br' bb_nr styleGreen,
              svgLine' bl' bb_nl styleGreen,
              svgLine' bb_nl nl styleGreen,
              svgLine' bb_nr nr styleGreen]


archtoneFBDim = ((7.5,0), (51.5,0), (0,438), (59,438))
stellaFBDim = ((7.5,0), (52.5,0), (0,408), (60,408))
cbgDim = ((0, 0), (38.5, 0), (0, 520), (38.5, 520))

h174Dim = ((7,0), (59,0), (0,460), (66,460))

-- 47 mm at nut, 60 mm at 390
ubassDim = ((6.5, 0), (53.5, 0), (0, 390), (60, 390))

-- 637mm scale length
acousticFBDim = ((7.5,0),(53.5,0),(0,438),(61,438))

genDim nutwidth endwidth len =
  let xoffset = (endwidth - nutwidth) / 2
  in
    ((xoffset, 0), (endwidth-xoffset, 0),
     (0, len), (endwidth, len))

sovereignDim = genDim 47 62.5 450

ibanez7FBDim = genDim 49 67.5 474

samickFBDim = genDim 45 58 477

-- add 3 mm to each side, subtract 4mm to compensate for zero fret, add 3 mm for more material
--twelvestringFBDim = genDim ((1.767*25.4)+6) ((2.362*25.4)+6) ((17.4 * 25.4)-1)

twelvestringFBDim = genDim (45.18 + 8) (60.1 + 4) (441.325)

hohnerFBDim = genDim (43.38 + 2.0) (57.86 + 2.0) (457.2 + 2.0)

redFBDim = ((5.5, 0), (49.5, 10), (0, 500), (55, 500))

arizonaFBDim = genDim (41 + 2) (55 + 2) (468.5 + 1)

doffFBDim = genDim (54.5 + 2) (68 + 2)  (468.1375 + 1)

testgtrFBDim = genDim 

lyxproDim = genDim (41.8 + 2) (55.18 + 2) (476.2 + 2)

renderClassicalKite =
  writeFile "data/fingerboard-classical-kite.svg" $
    drawfingerboardEt
      41 74 (map (*13) [0, 1, 2, 3, 4, 5])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0 || x == 3)
      637 637 h174Dim FretDoubleDots 10

renderAcousticFB = writeFile "data/fingerboard_ac_g.svg" $ drawfingerboard eadgbe_g just14 (4,1) 637 637 acousticFBDim FretNumber 10

renderArchtoneFB = writeFile "data/fingerboard.svg" $ drawfingerboard eadgbe_a  just16 (4,1) 635 635 cbgDim FretNumber 10

renderClassicalFB_G = writeFile "data/fingerboard-classical-g.svg" $ drawfingerboard eadgbe_g2 just15 (4,1) 637 637 h174Dim FretNumber 10 

renderUbassFB = writeFile "data/fingerboard-ubass-e.svg" $ drawfingerboard [(1,1), (4,3), (16,9), (64,27)] ubassScale (4,1) (264*2) (264*2) ubassDim FretNumber 10

renderClassicalFB_A = writeFile "data/fingerboard-classical-a.svg" $ drawfingerboard eadgbe_a2 just16_2 (4,1) 637 637 h174Dim FretNumber 10

renderClassicalFB_E = writeFile "data/fingerboard-classical-e.svg" $ drawfingerboard eadgbe_e2 just16_2 (4,1) 637 637 h174Dim FretNumber 10
renderClassicalFB_G2 = writeFile "data/fingerboard-classical-g2.svg" $ drawfingerboard eadgbe_g2 just16_2 (4,1) 637 637 h174Dim FretNumber 10

-- peavey predator + oversize
stratFBDim = genDim (43+5) (55+5) (473+10)

renderSovereignFB_G = writeFile "data/fingerboard-sovereign-g.svg" $ drawfingerboard eadgbe_g2 just16_2 (4,1) (318.7*2) (318.7*2) sovereignDim FretDot 10


renderRedJustKiteLike =
  writeFile "data/fingerboard-red-just-kite-like.svg" $
    drawfingerboard'
      five_string_thirds 
      (evenstrings ++ oddstrings)
      (\string pitch -> if (mod string 2) == 0 then elem pitch evenstrings else elem pitch oddstrings)
      (4,1)
      (682-4) (652-2) redFBDim FretSmallNumber 10
  where
    --set1 = [(7,8), (9,10), (15,16), (27,28), (1,1), (28,27), (16,15), (10,9), (8,7), (3,2), (14,9), (8,5), (5,3), (12,7)]
    --set2 = [(7,6), (6,5), (5,4), (9,7), (4,3), (7,4), (9,5), (15,8), (27,14), (2,1), (33,16), (15,7), (11,5), (16,7)]

    set1' = [(1,1), (3,2), (2,3), (81,64)]
    set2' = [(9,8), (8,9), (27,16), (16, 27)]
    -- set1'' = set1' ++ (map (stack (5,4)) set2') ++ (map (stack (4,5)) set2') ++ (map (stack (25,16)) set1') ++ (map (stack (16,25)) set1')
    -- set2'' = set2' ++ (map (stack (5,4)) set1') ++ (map (stack (4,5)) set1') ++ (map (stack (25,16)) set2') ++ (map (stack (16,25)) set2')

    set1'' = set1' ++ (map (stack (5,4)) set2') ++ (map (stack (4,5)) set2') ++ (map (stack (25,16)) set1') ++ (map (stack (16,25)) set1')
    set2'' = set2' ++ (map (stack (5,4)) set1') ++ (map (stack (4,5)) set1') ++ (map (stack (25,16)) set2') ++ (map (stack (16,25)) set2')

    set1 = set1''
    set2 = set2''

    evenstrings = (doubleOctaveDup set1) ++ (map (stack (1,2)) (doubleOctaveDup set2))
    oddstrings =  (doubleOctaveDup set2) ++ (map (stack (1,2)) (doubleOctaveDup set1))
    doubleOctaveDup scale = concat $ take 6 $ iterate (\xs -> map (stack (4,1)) xs) scale

renderStickJustKiteLike =
  writeFile "data/fingerboard-stick-just-kite-like.svg" $
    drawfingerboard'
      just_kite_stick
      (evenstrings ++ oddstrings)
      (\string pitch ->
        if (string < 6 && (mod string 2) == 1)
        then elem pitch evenstrings
        else elem pitch oddstrings)
      (4,1)
      (800) (800) stickFBDim FretSmallNumber 10
  where
    --set1 = [(7,8), (9,10), (15,16), (27,28), (1,1), (28,27), (16,15), (10,9), (8,7), (3,2), (14,9), (8,5), (5,3), (12,7)]
    --set2 = [(7,6), (6,5), (5,4), (9,7), (4,3), (7,4), (9,5), (15,8), (27,14), (2,1), (33,16), (15,7), (11,5), (16,7)]

    set1' = [(1,1), (3,2), (2,3), (81,64)]
    set2' = [(9,8), (8,9), (27,16), (16, 27)]
    -- set1'' = set1' ++ (map (stack (5,4)) set2') ++ (map (stack (4,5)) set2') ++ (map (stack (25,16)) set1') ++ (map (stack (16,25)) set1')
    -- set2'' = set2' ++ (map (stack (5,4)) set1') ++ (map (stack (4,5)) set1') ++ (map (stack (25,16)) set2') ++ (map (stack (16,25)) set2')

    set1'' = set1' ++ (map (stack (5,4)) set2') ++ (map (stack (4,5)) set2') ++ (map (stack (25,16)) set1') ++ (map (stack (16,25)) set1')
    set2'' = set2' ++ (map (stack (5,4)) set1') ++ (map (stack (4,5)) set1') ++ (map (stack (25,16)) set2') ++ (map (stack (16,25)) set2')

    set1 = set2''
    set2 = set1''

    evenstrings = (doubleOctaveDup set1) ++ (map (stack (1,2)) (doubleOctaveDup set2))
    oddstrings =  (doubleOctaveDup set2) ++ (map (stack (1,2)) (doubleOctaveDup set1))
    doubleOctaveDup scale = concat $ take 7 $ iterate (\xs -> map (stack (4,1)) xs) (map (stack (1,4)) scale)

    stickFBDim = genDim (13*10) (13*10) 400 

renderStellaKite =
  writeFile "data/fingerboard-stella-kite.svg" $
    drawfingerboardEt
      41 62 (map (*13) [0, 1, 2, 3, 4, 5])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0 || x == 3)
      612 612 stellaFBDim FretDoubleDots 10 

renderAcousticKite =
  writeFile "data/fingerboard-acoustic-kite.svg" $
    drawfingerboardEt
      41 66 (map (*13) [0, 1, 2, 3, 4, 5])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0 || x == 3)
      637 637 acousticFBDim FretInlay 10

renderIbanez7Kite =
  writeFile "data/fingerboard-ibanez7-kite.svg" $
    drawfingerboardEt
      41 76 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0)
      649 649 ibanez7FBDim FretInlay 10

renderSamickKite =
  writeFile "data/fingerboard-samick-kite.svg" $
    drawfingerboardEt
      41 75 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0 || x == 3)
      648 648 samickFBDim FretDoubleDots 10

renderTwelvestringKite =
  writeFile "data/fingerboard-twelvestring-kite.svg" $
    drawfingerboardEt
      41 65 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0)
      (658.8125) (658.8125) twelvestringFBDim NoDecoration 30

renderHohnerKite =
  writeFile "data/fingerboard-hohner-kite.svg" $
    drawfingerboardEt
      41 71 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0)
      (25.5*25.4) (25.5*25.4) hohnerFBDim NoDecoration 30

renderArizonaKite =
  writeFile "data/fingerboard-arizona-kite.svg" $
    drawfingerboardEt
      41 70 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0)
      648.4 648.4 hohnerFBDim FretDoubleDots 30

renderStratKite =
  writeFile "data/fingerboard-strat-kite.svg" $
    drawfingerboardEt
      41 80 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0)
      647.7 647.7 stratFBDim FretDoubleDots 30

renderStratKiteHalfFret =
  writeFile "data/fingerboard-strat-halffret-kite.svg" $
    drawfingerboardEt
      41 80 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0 || x == 3)
      647.7 647.7 stratFBDim FretDoubleDots 30

renderDoffKite =
  writeFile "data/fingerboard-doff-kite.svg" $
    drawfingerboardEt
      41 74 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0)
      647.7 647.7 doffFBDim FretDoubleDots 30

renderTripleKite =
  writeFile "data/fingerboard-tripledot-kite.svg" $
    drawfingerboardEt
      41 74 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0)
      647.7 647.7 doffFBDim FretTripleDots 30

renderKiteTestscale =
  writeFile "data/fingerboard-testscale.svg" $
    drawfingerboardEt
      41 74 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0)
      647.7 647.7 doffFBDim FretTripleDots 30

renderlyxprokite =       
  writeFile "data/lyxprostrat.svg" $
    drawfingerboardEt
      41 80 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0 || x == 1 || x == 3)
      647.7 647.7 lyxproDim FretTripleDots 30      
      
copyrenderStratKiteHalfFret =
  writeFile "data/fingerboard-strat-halffret-kite.svg" $
    drawfingerboardEt
      41 80 (map (*13) [0, 1, 2, 3, 4, 5, 6])
      (\x -> let pc = mod x 41 in pc == 0 || pc == 17 || pc == 24)
      (\x -> mod x 2 == 0 || x == 1)
      647.7 647.7 stratFBDim FretDoubleDots 30      
      
drawFingerboards =
  do let scale = just20
     writeFile "data/fingerboard_at_e.svg" $ drawfingerboard eadgbe_e scale (3,1) 636 636 archtoneFBDim FretNumber 10
     writeFile "data/fingerboard_at_a.svg" $ drawfingerboard eadgbe_a scale (3,1) 636 636 archtoneFBDim FretNumber 10
     writeFile "data/fingerboard_at_d.svg" $ drawfingerboard eadgbe_d scale (3,1) 636 636 archtoneFBDim FretNumber 10
     writeFile "data/fingerboard_st_e.svg" $ drawfingerboard eadgbe_e scale (3,1) 612 612 stellaFBDim FretNumber 10
     writeFile "data/fingerboard_st_a.svg" $ drawfingerboard eadgbe_a scale (3,1) 612 612 stellaFBDim FretNumber 10
     writeFile "data/fingerboard_st_d.svg" $ drawfingerboard eadgbe_d scale (3,1) 612 612 stellaFBDim FretNumber 10


delScale = [(1,1), (16,15), (9,8), (7,6), (6,5), (5,4), (4,3), (45,32), (3,2), (8,5), (5,3), (7,4), (9,5), (15,8), (2,1)]

renderDelFB =  writeFile "data/delfb.svg" $ drawfingerboard [(1,1), (4,3), (3,2), (2,1)] delScale (4,1) 636 636 cbgDim FretNumber 10

-- How many times does "prime" appear as a factor?
-- (a counts as positive, b negative)
factorcount :: Integer -> Interval -> Integer
factorcount prime (a,b)
 | prime <= 1 = error "factorcount requires prime of 2 or more"
 | otherwise = (go a 0) - (go b 0)
    where
      go x acc =
        if mod x prime == 0
        then go (div x prime) (acc+1)
        else acc

octaveShift :: Interval -> Integer -> Interval
octaveShift (a,b) oct
  | oct >= 0 = normalize $ (a * (2^oct), b) 
  | oct < 0 = normalize $ (a, b * (2^(-oct)))

same :: Interval -> Interval -> Bool
same n1 n2 =
  let (a1, b1) = normalize n1
      (a2, b2) = normalize n2
  in
     (a1 == a2) && (b1 == b2)

vstretch :: Double -> (Pt2D -> Pt2D, Pt2D -> Pt2D)
vstretch scale = (\(x, y) -> (x, y*scale), \(x, y) -> (x, y/scale))

box :: Pt2D -> Pt2D -> [(Pt2D)]
box (x1, y1) (x2, y2) =
  [(x1, y1), (x1, y2), (x2, y2), (x2, y1)]

drawlattice :: Integer -> Integer -> String
drawlattice xfactor yfactor =
  let n = 4
      cellval r c =
        let a = (xfactor ^ (r+n))
            b = (yfactor ^ (c+n))
        in stack (a,b) (yfactor ^ n, xfactor ^n)
      cell r c = "<td>" ++ (showi (cellval r c)) ++ "</td>"
      row r = "<tr>" ++ (concatMap (cell r) [(-n)..n]) ++ "</tr>\n"
      rows  = concatMap row [(-n)..n]
  in 
   "<table>\n" ++ rows ++ "</table>\n"

drawlattice_normal :: Integer -> Integer -> String
drawlattice_normal xfactor yfactor =
  let n = 4
      cellval r c =
        let a = (xfactor ^ (r+n))
            b = (yfactor ^ (c+n))
        in pitchclass $ stack (a,b) (yfactor ^ n, xfactor ^n)
      cell r c = "<td>" ++ (showi (cellval r c)) ++ "</td>"
      row r = "<tr>" ++ (concatMap (cell r) (reverse [(-n)..n])) ++ "</tr>\n"
      rows  = concatMap row (reverse [(-n)..n])
  in 
   "<table>\n" ++ rows ++ "</table>\n"


gear :: Pt2D -> Integer -> Double -> Double -> Double -> String
gear center teeth pitch toothsize innerradius =
  let diameter = (fromIntegral teeth) * pitch
      radius = diameter / (2*pi)
      style = styleSection
  in
    (concat
      [svgCircle c (toothsize/2) style
        | phase <- map (\t -> (fromIntegral t) / (fromIntegral teeth)) [0..(teeth-1)]
        , let c = ptRotate center phase (ptAdd center (0, radius)) ]
    ) ++
    (svgCircle center radius style) ++
    (svgCircle center innerradius style)

gears =
  let pitch = 2.07
      shaft = 3 -- 2.6 -- 2.3 -- 3
      toothsize = 1.3
      g pos teeth = gear pos teeth pitch toothsize shaft
      gs =
       [ g (100,100) 23 -- 113
       , g (200,100) 29
       , g (100,200) 103 -- 19
       , g (200,200) 83 -- 25
       , g (150,150) 31
       ]
  in
     svgWrap (concat gs) (300,300) "mm"

dicecalc :: Integer -> Integer-> [(Integer, Integer)]
dicecalc die sides =
  let vals = [1..sides]
      crossprod 1 = vals
      crossprod n = [a+b | a <- vals, b <- crossprod (n-1)]
  in map (\x -> (head x, fromIntegral $ length x)) (group $ sort $ crossprod die)

drawGears :: IO ()
drawGears =
  writeFile "data/gears.svg" gears

isPowN :: Integer -> Integer -> Bool
isPowN n 1 = True
isPowN n x =
  if mod x n > 0
  then False
  else isPowN n (div x n)

main :: IO ()
main = return ()
